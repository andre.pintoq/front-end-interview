import React, { useState, useEffect } from 'react';
import generateMessage, { Message } from './Api';
import Container from '@mui/material/Container';
import { ErrorToast } from "./components/ErrorToast";
import { Header } from "./components/Header";
import { Context } from "./components/Context";
import { Columns } from "./components/Columns";



const App: React.FC<{}> = () => {
  const [messages, setMessages] = useState<Message[]>([]);
  const [stop, setStop] = useState(false);
  const [snackBarContent, setSnackBarkContent] = useState({
    open: false,
    content: "",
  });

  useEffect(() => {
    if (!stop) {

      const cleanUp = generateMessage((message: Message) => {
        if (message.priority === 0) {
          setSnackBarkContent({
            open: true,
            content: message.message,
          });
        }
        setMessages(oldMessages => [...oldMessages, message]);
      });

      return cleanUp;
    }
  }, [setMessages, stop]);

  const clearMessages = () => {
    setMessages([]);
  }

  const stopMessages = () => {
    setStop(!stop);
  }

  const removeMessage = (message: Message) => {
    const nextMessages = messages.filter(msg => msg.message !== message.message)
    setMessages(nextMessages);
  }

  const handleSnackBarClose = (event: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setSnackBarkContent({
      open: false,
      content: "",
    });
  }

  return (
    <Context.Provider value={{
      removeMessage,
    }}>
      <Container>
        <Header stop={stop} clearMessages={clearMessages} stopMessages={stopMessages} />
        <Columns messages={messages} />
        <ErrorToast
          open={snackBarContent.open}
          content={snackBarContent.content}
          handleClose={handleSnackBarClose}
        />
      </Container>
    </Context.Provider>

  );
}

export default App;
