import React, { FC } from 'react';

import Snackbar from '@mui/material/Snackbar';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';

type ErrorToastProps = {
    open: boolean;
    handleClose: (event: React.SyntheticEvent | Event, reason?: string) => void;
    content: string;
}

export const ErrorToast: FC<ErrorToastProps> = (props) => {
    const { open, handleClose, content } = props;
    const action = (
        <React.Fragment>
            <IconButton
                size="small"
                aria-label="close"
                color="inherit"
                onClick={handleClose}
            >
                <CloseIcon fontSize="small" />
            </IconButton>
        </React.Fragment>
    );


    return (
        <Snackbar
            open={open}
            autoHideDuration={2000}
            onClose={handleClose}
            message={content}
            action={action}
        />
    )
}