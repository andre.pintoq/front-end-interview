import React from "react";
import { Message } from '../Api';
import { MessageItem } from "./MessageItem"
import { StyledColumn, StyledTitle, StyledCount } from "./StyledComponents"

type ColumnProps = {
    list: Message[];
    title: string;
};

export const ColumnItem: React.FC<ColumnProps> = (props) => {
    const { list, title } = props;
    return (
        <StyledColumn>
            <StyledTitle>{title}</StyledTitle>
            <StyledCount>Count {list.length}</StyledCount>
            {list?.map?.((msg) => <MessageItem key={msg.message} msg={msg} />)}
        </StyledColumn>
    )
}
