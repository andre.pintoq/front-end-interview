import styled from "styled-components";
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';

const colorPallete = [
    "#F56236",
    "#FCE788",
    "#88FCA3",
];

export const ColumnContainer = styled(Container)`
    display: flex;
    flex-direction: row;
    
`;

export const HeaderContainer = styled(Container)`
    display: flex;
    flex: 1;
    align-items: center;
    justify-content: center;
`;

export const StyledButton = styled(({ color, ...otherProps }) => <Button {...otherProps} />)`
    color: ${props => props.color};
    margin: 10px 2px;
    padding: 5px 15px;
    background-color: #88FCA3;
    font-weight: bold;
`;

export const StyledColumn = styled.div`
    flex: 2;
`;

export const StyledTitle = styled.h3`
    margin-bottom: 0px;
`;

export const StyledCount = styled.div`
    margin-bottom: 10px;
`;

export const MessageItemContainer = styled(({ priority, ...otherProps }) => <Card {...otherProps} />)`
    margin: 0px 10px 10px 0px;
    padding: 20px;
    border-radius: 3px;
    background-color: ${props => colorPallete[props.priority]};
`;

export const StyledClear = styled.div`
    text-align: right;
  `;