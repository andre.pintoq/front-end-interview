import React from "react";
import Container from '@mui/material/Container';
import { HeaderContainer, StyledButton } from "./StyledComponents"

type HeaderProps = {
    stopMessages: () => void;
    clearMessages: () => void;
    stop: boolean
};

export const Header: React.FC<HeaderProps> = (props) => {
    const { stopMessages, clearMessages, stop } = props;
    return (
        <Container>
            <h2>Coding Challenge</h2>
            <HeaderContainer>
                <StyledButton variant="contained" color="#262626" onClick={stopMessages}>{stop ? "Start" : "Stop"}</StyledButton>
                <StyledButton variant="contained" color="#262626" onClick={clearMessages}>Clear</StyledButton>
            </HeaderContainer>
        </Container>
    )
}
