import React from 'react';
import  { Message } from '../Api';
import { ColumnItem } from "./ColumnItem"
import { ColumnContainer } from "./StyledComponents";


type ColumnsProps = {
    messages: Message[];
}
export const Columns: React.FC<ColumnsProps> = (props) => {
  const { messages } = props;


  const columns = [
    {
      title: "Error Type 1",
      list: messages?.filter(message => message.priority === 0),
    },
    {
      title: "Warning Type 2",
      list: messages?.filter(message => message.priority === 1),
    },
    {
      title: "Info Type 3",
      list: messages?.filter(message => message.priority === 2),
    },
  ]

  return (
    <ColumnContainer >
          {
            columns.map(column =>
              <ColumnItem
                key={column.title}
                title={column.title}
                list={column.list}
              />)
          }
    </ColumnContainer>

  );
}