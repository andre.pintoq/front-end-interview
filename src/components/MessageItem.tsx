import React, { useContext } from "react";
import { Message } from '../Api';
import { MessageItemContainer, StyledClear } from "./StyledComponents"
import { Context, ContextType } from "./Context";

type MessageItemProps = {
    msg: Message;
};

export const MessageItem: React.FC<MessageItemProps> = (props) => {
    const { msg } = props;
    const { removeMessage } = useContext<ContextType>(Context);
    return (
        <MessageItemContainer priority={msg.priority} key={msg.message}>
            <div key={msg?.message}>{msg?.message}</div>
            <StyledClear onClick={() => removeMessage(msg)}>Clear</StyledClear>
        </MessageItemContainer>
    )
}
