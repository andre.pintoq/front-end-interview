import { createContext } from 'react';
import { Message } from '../Api';

export const Context = createContext<ContextType>({
    removeMessage: (msg) => {}
});

export type ContextType = {
    removeMessage: (msg: Message) => void;
}