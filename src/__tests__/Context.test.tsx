import React from 'react'
import {render, fireEvent } from '@testing-library/react'
import '@testing-library/jest-dom'
import { Context } from '../components/Context'
import { Columns } from '../components/Columns';

test('Context value function has been called 1 time', () => {
    const messages = [{ message: "dolor sit amet", priority: 0}, { message: "amet sit dolor", priority: 1}, { message: "sit dolor amet", priority: 2}]

    const removeMessage = jest.fn();
    const { getAllByText } = render(
        <Context.Provider value={{ removeMessage }}>
          <Columns messages={messages} />
        </Context.Provider>
    );
    
    const clearMessage = getAllByText("Clear")[1];
    expect(clearMessage).not.toBe(null)
  })