import React from 'react'
import {render, fireEvent } from '@testing-library/react'
import '@testing-library/jest-dom'
import { Context } from '../components/Context'
import { Columns } from '../components/Columns';

test('Context default value is a function', () => {
    const messages = [{ message: "dolor sit amet", priority: 0}, { message: "amet sit dolor", priority: 1}, { message: "sit dolor amet", priority: 2}]

    const removeMessage = jest.fn();
    const { getByText } = render(
        <Context.Provider value={{ removeMessage }}>
          <Columns messages={messages} />
        </Context.Provider>
    );
    
    const messageError = getByText("dolor sit amet");
    const messageWarning = getByText("amet sit dolor");
    const messageInfo = getByText("sit dolor amet");

    expect(messageError).not.toBe(null)
    expect(messageWarning).not.toBe(null)
    expect(messageInfo).not.toBe(null)
  })