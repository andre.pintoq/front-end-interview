import React from 'react'
import {render } from '@testing-library/react'
import '@testing-library/jest-dom'
import { MessageItem } from '../components/MessageItem';

test('Message Item to have message', () => {
    const { getByText } = render(
          <MessageItem index={0} msg={{ message: "Lorem ipsum dolor sit amot", priority: 0 }} />
    );

    const message = getByText("Lorem ipsum dolor sit amot");

    expect(message).not.toBe(null)
})